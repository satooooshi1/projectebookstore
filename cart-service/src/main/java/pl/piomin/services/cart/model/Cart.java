package pl.piomin.services.cart.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

@Document(collection = "cart")
public class Cart {

    @Id
    private String id;
    private String customerId;
    private String productId;
    //private Product product;
    //MEMO: date handling, format
    //https://blogg.kantega.no/webapp-with-create-react-app-and-spring-boot/
    private Date date;//added date
    private CartType type;

    public Cart() {

    }

    public Cart(String customerId, String productId, CartType type) {
        this.customerId=customerId;
        this.productId=productId;
        this.type=type;
    }

    public Cart(String customerId, String productId, Date date, CartType type){
        this.customerId=customerId;
        this.productId=productId;
        this.date=date;
        this.type=type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId(){
        return customerId;
    }

    public void setCustomerId(String customerId){
        this.customerId=customerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date=date;
    }

    public CartType getType() {
        return type;
    }

    public void setType(CartType type){
        this.type=type;
    }

}

