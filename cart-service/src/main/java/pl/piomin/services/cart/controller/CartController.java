package pl.piomin.services.cart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.piomin.services.cart.contract.CartService;
import pl.piomin.services.cart.model.Cart;
import pl.piomin.services.cart.model.CartType;
import pl.piomin.services.cart.repository.CartRepository;
import pl.piomin.services.product.model.Product;

import pl.piomin.services.cart.client.ProductClient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class CartController implements CartService {

	@Autowired
	ProductClient productClient;

	@Autowired
	CartRepository repository;

	
	public Cart addToCart(@RequestBody Cart cart) {

		//System.out.println(cart.getCustomerId());
		//System.out.println(cart.getProductId());
		List<Cart> cartOfCustomerId=repository.findByCustomerId(cart.getCustomerId());

		//if(cartOfCustomerId!=null)
		for(Cart item : cartOfCustomerId){
			//System.out.println(item.getProductId());
			//already in cart
			if(item.getProductId().equals(cart.getProductId())){//compare strings!!
				return null; //this should be handled as error in axios in frontend!!
			}
		}

		Date date=new Date();
		cart.setDate(date);
		cart.setType(CartType.CART);
		return repository.save(cart);
	}
	

	public Cart findById(@PathVariable("id") String id) {
		return repository.findById(id);
	}

	public List<Product> findByCustomerIdWithProductsCart(@PathVariable("customerId") String customerId){
		List<Cart> carts=repository.findByCustomerId(customerId);

		List<String>productIdsCart=new ArrayList<>();
		for(Cart cart : carts){
			if(cart.getType()==CartType.CART)
				productIdsCart.add(cart.getProductId());

		}

		List<Product> productsCart=productClient.find(productIdsCart);
		return productsCart;
	}

	public List<Product> findByCustomerIdWithProductsSeelater(@PathVariable("customerId") String customerId){
		List<Cart> carts=repository.findByCustomerId(customerId);

		List<String>productIdsSeelater=new ArrayList<>();
		for(Cart cart : carts){
			if(cart.getType()==CartType.SEELATER)
				productIdsSeelater.add(cart.getProductId());
		}

		List<Product> productsSeelater=productClient.find(productIdsSeelater);
		return productsSeelater;
	}


	public List<Cart> findAll(){return repository.findAll();}

	public void delete(@PathVariable("productId") String productId, @PathVariable("customerId") String customerId) {
		repository.deleteByProductIdAndCustomerId(productId, customerId);
	}

}
