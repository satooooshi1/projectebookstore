package pl.piomin.services.cart.contract;

import org.springframework.web.bind.annotation.*;
import pl.piomin.services.cart.model.Cart;
import pl.piomin.services.product.model.Product;

import java.util.List;

public interface CartService {


	@PostMapping("/v1")
	Cart addToCart(@RequestBody Cart cart);
	

	@GetMapping("/v1/{id}")
	Cart findById(@PathVariable("id") String id);

	@GetMapping("/v1")
	List<Cart> findAll();

	@GetMapping("/v1/cartof/{customerId}")
	List<Product> findByCustomerIdWithProductsCart(@PathVariable("customerId") String customerId);

	@GetMapping("/v1/seelaterof/{customerId}")
	List<Product> findByCustomerIdWithProductsSeelater(@PathVariable("customerId") String customerId);


	@DeleteMapping("/v1/{productId}/{customerId}")
	void delete(@PathVariable("productId") String productId, @PathVariable("customerId") String customerId);
	
}
