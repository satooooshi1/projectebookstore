package pl.piomin.services.cart.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import pl.piomin.services.cart.model.Cart;
import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.model.Product;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/*
public class CartRepository {

    private List<Cart> carts = new ArrayList<>();

    public Cart add(Cart cart) {
        cart.setId((long) (carts.size()+1));
        //MEMO: programming of date, format
        //https://www.sejuku.net/blog/24887
        cart.setDate(new Date());
        carts.add(cart);
        return cart;
    }

    public Cart findById(Long id) {
        Optional<Cart> cart = carts.stream().filter(p -> p.getId().equals(id)).findFirst();
        if (cart.isPresent())
            return cart.get();
        else
            return null;
    }

    public List<Cart> findByCustomerId(Long customerId){
        return carts.stream().filter(a -> a.getCustomerId().equals(customerId)).collect(Collectors.toList());

    }

    public List<Cart> findAll(){
        return carts;
    }

    public void delete(Long id) {
        carts.remove(id.intValue());
    }


}
*/

public interface CartRepository extends MongoRepository<Cart, String> {

    public List<Cart> findAll();
    public Cart findById(String id);
    List<Cart> findByCustomerId(String customerId);
    public void deleteByProductIdAndCustomerId(String  productId, String customerId);
}