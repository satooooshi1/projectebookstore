package pl.piomin.services.cart;

import com.netflix.discovery.converters.Auto;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.piomin.services.cart.model.Cart;
import pl.piomin.services.cart.model.CartType;
import pl.piomin.services.cart.repository.CartRepository;
import pl.piomin.services.product.model.Product;
import pl.piomin.services.product.repository.ProductRepository;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

@CrossOrigin
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
//@EnableSwagger2
public class CartApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(CartApplication.class).web(true).run(args);
	}

	@Autowired
	CartRepository repository;
/*
	@Bean
	CartRepository repository() {
		//CartRepository repository = new CartRepository();
		repository.save(new Cart("100","100",new Date(), CartType.CART));
		repository.save(new Cart("100","200", new Date(), CartType.CART));
		repository.save(new Cart("100","300", new Date(), CartType.CART));
		return repository;
	}
*/
	//MEMO: cant use feign and swagger at the same time
	//Ex. when @RequestMapping("/product") cant feign from cart-service until erase @RequestMapping("/product") annotation
	//when
	//http://localhost:8093/v2/api-docs
	@Bean
	public Docket api() throws IOException, XmlPullParserException {
		MavenXpp3Reader reader = new MavenXpp3Reader();
		Model model = reader.read(new FileReader("pom.xml"));
		ApiInfoBuilder builder = new ApiInfoBuilder()
				.title("Order Service Api Documentation")
				.description("Documentation automatically generated")
				.version(model.getVersion())
				.contact(new Contact("Satoshi Aikawa", "gitlab.com/satooooshi1/springcloud", "TBU.."));
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("pl.piomin.services.cart.controller"))
				.paths(PathSelectors.any()).build()
				.apiInfo(builder.build());
	}
//http://localhost:8080/cart/swagger-ui.html#/cart45controller
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				//registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:3000");
				registry.addMapping("/**").allowedOrigins("http://localhost:3000").allowedMethods("GET","POST","PUT", "DELETE");
			}
		};
	}

}
