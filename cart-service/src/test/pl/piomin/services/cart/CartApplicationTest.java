package pl.piomin.services.cart;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
//@EnableSwagger2
public class CartApplicationTest {

    @Test
    public void contextLoads() {
    }
}