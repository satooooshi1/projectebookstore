package pl.piomin.services.cart;
import static io.specto.hoverfly.junit.core.SimulationSource.dsl;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.startsWith;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import io.specto.hoverfly.junit.rule.HoverflyRule;
import pl.piomin.services.cart.model.Cart;
import pl.piomin.services.cart.model.CartType;
import pl.piomin.services.product.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles({"componenttest", "no-discovery"})
public class CartComponentTest {

    @Autowired
    TestRestTemplate restTemplate;

    @ClassRule
    public static HoverflyRule hoverflyRule = HoverflyRule
            .inSimulationMode(dsl(
                    //for test case testFindByCustomerIdWithProductsCart
                    service("product-service:8080")
                            .andDelay(200, TimeUnit.MILLISECONDS).forAll()
                            .post(startsWith("/v1/ids")).anyBody()
                            .willReturn(success("[{\"id\":\"1\",\"name\":\"Test1\",\"price\":1000}]", "application/json"))))
            .printSimulationData();

    @Test
    public void testAdd() {
        Cart cart = new Cart("1","1",new Date(), CartType.CART);
        cart = restTemplate.postForObject("/v1", cart, Cart.class);
        Assert.assertNotNull(cart);
    }

    @Test
    public void testDelete() {

        //given
        Cart cart = new Cart("10001","10001",new Date(), CartType.CART);
        cart = restTemplate.postForObject("/v1", cart, Cart.class);

        //MEMO: test delete
        //http://pppurple.hatenablog.com/entry/2016/10/24/031642

        //when, then
        restTemplate.delete("/1/10001/10001");


    }
/*FAILED because of http request to other service (product service)
    @Test
    public void testFindByCustomerIdWithProductsCart() {
        //given
        Cart cart = new Cart("1","1",new Date(), CartType.CART);
        cart = restTemplate.postForObject("/v1", cart, Cart.class);


        //when
        //ATTENTION: need hoverfly for ProductClient.find(productIdsCart); !!
        ResponseEntity<List<Product>> productResponse = restTemplate.exchange(
                "/v1/cartof/1",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Product>>() {});
        //then
        Assert.assertTrue(productResponse.getBody().size()!=0);
    }
*/
    @Test
    public void testFindById() {
        //given
        Cart cart = new Cart("1","1",new Date(), CartType.CART);
        cart.setId("1");
        cart = restTemplate.postForObject("/v1", cart, Cart.class);


        //when
        ResponseEntity<List<Cart>> cartResponse = restTemplate.exchange(
                "/v1",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Cart>>() {});
        //then
        Assert.assertTrue(cartResponse.getBody().size()!=0);
    }
}