package pl.piomin.services.cart.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import pl.piomin.services.cart.model.Cart;
import pl.piomin.services.cart.model.CartType;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@DataMongoTest
public class CartRepositoryTest {

    @Autowired
    private CartRepository repository;

    //given
    private static Cart cart = new Cart("1","1",new Date(), CartType.CART);
    @Before// each test
    public void dataSetup() {

        cart.setId("1");
        repository.save(cart);

    }


    @Test
    public void testFindAll() {

        List<Cart> found=repository.findAll();
        assertThat(found.get(0).getId()).isEqualTo(cart.getId());
    }

    @Test
    public void findById() {
        Cart found=repository.findById(cart.getId());
        assertThat(found.getId()).isEqualTo(cart.getId());
    }

    @Test
    public void findByCustomerId() {
        List<Cart> found=repository.findByCustomerId(cart.getCustomerId());
        assertThat(found.get(0).getId()).isEqualTo(cart.getCustomerId());
    }

    @Test
    public void deleteByProductIdAndCustomerId() {
        //when
        repository.deleteByProductIdAndCustomerId(cart.getProductId(), cart.getCustomerId());
        //then
        Cart found=repository.findById(cart.getId());
        assertThat(found).isNull();

    }
}

