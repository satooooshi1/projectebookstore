package pl.piomin.services.product.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.piomin.services.product.contract.ProductService;
import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.model.Product;
import pl.piomin.services.product.repository.ProductRepository;

@RestController
//@RequestMapping("/product")
public class ProductController implements ProductService {

	@Autowired
	ProductRepository repository;

	public List<Product> findAll(){return repository.findAll();}

	public List<Product> findByKeyword(@PathVariable  String keyword){return repository.findByNameLike(keyword);}

	//MEMO: ArrayList等で値が重複しないListを作ろうとすると自分で処理を書かなければならずとても面倒です。
	//https://stackoverflow.com/questions/29670116/remove-duplicates-from-a-list-of-objects-based-on-property-in-java-8
	//this is new way
	//https://javadeveloperzone.com/java-basic/remove-duplicate-custom-objects-arraylist-java/
	public List<Product> findByGenres(@RequestBody List<GenreType> genres){
		List<Product> products= new ArrayList<>();
		for(GenreType genre : genres ){
			products.addAll(repository.findByGenre(genre));
		}

		HashSet<Object> unique=new HashSet<>();
		products.removeIf(p->!unique.add(p.getId()));
		return new ArrayList <Product> (products);
	}

	public Product add(@RequestBody Product product) {
		return repository.save(product);
	}
	
	public Product update(@RequestBody Product product) {
		return repository.save(product);
	}
	
	public Product findById(@PathVariable("id") String id) {
		return repository.findById(id);
	}
	
	public List<Product> find(@RequestBody List<String> ids) {
		List<Product> products= new ArrayList<>();
		for(String id : ids ){
			products.add(repository.findById(id));
		}
		return products;
	}

	public void delete(@PathVariable("id") String id) {
		repository.delete(id);
	}
	
}
