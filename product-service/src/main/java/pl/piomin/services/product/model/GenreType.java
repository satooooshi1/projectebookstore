package pl.piomin.services.product.model;

public enum GenreType {

	WebDevelopment , Data, Programming, CloudNetworking, Business,
	GameDevelopment, Mobile, Security, Hardware;
	
}
