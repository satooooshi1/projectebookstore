package pl.piomin.services.product.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "product")
public class Product {

	@Id
	private String id;
	private String name;
	private String author;
	private Date date;
	private String description;
	private List<GenreType> genres;
	private int price;

	public Product() {

	}

	public Product(String name, int price) {
		this.name = name;
		this.price = price;
	}

	public Product(String name, String author, int price){
		this.name=name;
		this.author=author;
		this.price=price;
	}

	public Product(String name, String author, Date date, String description, List<GenreType> genres, int price){
		this.name = name;
		this.author = author;
		this.date = date;
		this.description = description;
		this.genres = genres;
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor(){
		return author;
	}

	public void setAuthor(String author){
		this.author=author;
	}

	public Date getDate(){
		return date;
	}

	public void setDate(Date date){
		this.date=date;
	}

	public String getDescription(){
		return description;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public List<GenreType> getGenres(){
		return genres;
	}

	public void setGenres(List<GenreType> genres){
		this.genres=genres;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
