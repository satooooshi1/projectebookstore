package pl.piomin.services.product;

import com.netflix.discovery.converters.Auto;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.piomin.services.product.model.Product;
import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.repository.ProductRepository;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin
@EnableSwagger2
@SpringBootApplication
@EnableDiscoveryClient
public class ProductApplication {

	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(ProductApplication.class).web(true).run(args);
	}

	@Autowired
	ProductRepository repository;

	@Bean
	ProductRepository repository() {
		//ProductRepository repository = new ProductRepository();



		ArrayList<GenreType> genres = new ArrayList<GenreType>();
		genres.add(GenreType.Programming);
		genres.add(GenreType.GameDevelopment);
		genres.add(GenreType.Mobile);
		repository.save(new Product("Applied Machine Learning with Python",
				"By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
				genres,
				18));
		repository.save(new Product("Apache Camel Essentials",
				"Prajod Surendran V",new Date(),"Learn the key features and implementation strategies of the Apache Camel Enterprise Integration framework" ,
				genres,
				10));
		repository.save(new Product("Learn the key features and implementation strategies of the Apache Camel Enterprise Integration framework",
				"Usama Wahab Khan",new Date(),"Learn the tips and tricks of creating analytical reports and dashboards for your business in no time.",
				genres,
				45));
		repository.save(new Product("Go Programming Cookbook - Second Edition",
				"Aaron Torres\n",new Date(),"Tackle the trickiest of problems in Go programming with this highly practical guide.",
				genres,
				40));
		return repository;
	}

	//MEMO: cant use feign and swagger at the same time
	//Ex. when @RequestMapping("/product") cant feign from cart-service until erase @RequestMapping("/product") annotation
	//when
	//http://localhost:8093/v2/api-docs
	@Bean
	public Docket api() throws IOException, XmlPullParserException {
		MavenXpp3Reader reader = new MavenXpp3Reader();
		Model model = reader.read(new FileReader("pom.xml"));
		ApiInfoBuilder builder = new ApiInfoBuilder()
				.title("Order Service Api Documentation")
				.description("Documentation automatically generated")
				.version(model.getVersion())
				.contact(new Contact("Satoshi Aikawa", "gitlab.com/satooooshi1/springcloud", "TBU.."));
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("pl.piomin.services.product.controller"))
				.paths(PathSelectors.any()).build()
				.apiInfo(builder.build());
	}
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				//registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:3000");
				registry.addMapping("/**").allowedOrigins("http://localhost:3000").allowedMethods("GET","POST","PUT", "DELETE");
			}
		};
	}

}
