package pl.piomin.services.product.contract;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.model.Product;


public interface ProductService {

	@GetMapping("/v1")
	List<Product>findAll();

	@GetMapping("/v1/keyword/{keyword}")
	List<Product> findByKeyword(@PathVariable("keyword") String keyword);

	@PostMapping("/v1/genres")
	List<Product> findByGenres(@RequestBody List<GenreType> genres);

	@PostMapping("/v1")
	Product add(@RequestBody Product product);
	
	@PutMapping("/v1")
	Product update(@RequestBody Product product);
	
	@GetMapping("/v1/{id}")
	Product findById(@PathVariable("id") String id);
	
	@PostMapping("/v1/ids")
	List<Product> find(@RequestBody List<String> ids);

	@DeleteMapping("/v1/{id}")
	void delete(@PathVariable("id") String id);

}
