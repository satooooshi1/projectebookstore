package pl.piomin.services.product.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.mongodb.repository.Query;
import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.model.Product;
/*
public class ProductRepository {

	private List<Product> products = new ArrayList<>();

	public List<Product> findAll(){return products;}

	//MEMO: find List
	//https://dzone.com/articles/extracting-elements-java
	public List<Product> findByKeyword(String keyword){
		List<Product> productsByKeyword =
				//Create a Stream from the personList
				products.stream().
						//filter the element to select only those with age >= 30
								filter(u -> u.getName().toLowerCase().contains(keyword.toLowerCase())).
						//put those filtered elements into a new List.
								collect(Collectors.toList());
		return productsByKeyword;
	}

	//determine whether an array contains a particular value
	//https://stackoverflow.com/questions/1128723/how-do-i-determine-whether-an-array-contains-a-particular-value-in-java
	public List<Product> findByGenres(List<GenreType> genres){

		List<Product> productsByGenre=products.stream().filter(b -> b.getGenres().contains(genres.get(0))).collect(Collectors.toList());
		return productsByGenre;
	}
	
	public Product add(Product product) {
		product.setId((long) (products.size()+1));
		products.add(product);
		return product;
	}
	
	public Product update(Product product) {
		products.set(product.getId().intValue() - 1, product);
		return product;
	}
	
	public Product findById(Long id) {
		Optional<Product> product = products.stream().filter(p -> p.getId().equals(id)).findFirst();
		if (product.isPresent())
			return product.get();
		else
			return null;
	}
	
	public void delete(Long id) {
		products.remove(id.intValue());
	}
	
	public List<Product> find(List<Long> ids) {
		return products.stream().filter(p -> ids.contains(p.getId())).collect(Collectors.toList());
	}
}
*/
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.piomin.services.product.model.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

	//MEMO: MongoDB Query methods
	//https://docs.spring.io/spring-data/mongodb/docs/1.2.0.RELEASE/reference/html/mongo.repositories.html
	public List<Product> findAll();
	public List<Product> findByNameLike(String keyword);
	//MEMO: search from array
	//
	@Query(value = "{ 'genres' : {$all : [?0] }}")
	public List<Product> findByGenre(GenreType genre);
	public Product findById(String id);
}
