package pl.piomin.services.product.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.model.Product;
import pl.piomin.services.product.repository.ProductRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//https://examples.javacodegeeks.com/core-java/junit/junit-mockito-when-thenreturn-example/
import static org.mockito.Mockito.when;
/*
public class ProductControllerTest {

}
*
*
*/

//MEMO: test examples
//https://github.com/harshulvarshney/SpringBoot_REST_Mongo/blob/master/sample-bird-app/src/test/java/com/harshul/sample_bird_app/BirdAppControllerTest.java

//THIS IS UNIT TEST
@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    MockMvc mvc;
    @MockBean
    ProductRepository repository;

    @Test
    public void testAdd() throws Exception {

        ArrayList<GenreType> genres = new ArrayList<GenreType>();
        genres.add(GenreType.Programming);
        genres.add(GenreType.GameDevelopment);
        genres.add(GenreType.Mobile);
        Product product = new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18);

        //MEMO:given-when-then
        //https://blog.codecentric.de/en/2017/09/given-when-then-in-junit-tests/
        //
        //MOCK: basically can mock component whose test have already passed before at somewhere else
        //
        //Given-When-Then test writing
        //
        // Given
        //Preparation
        //Here objects are created that are required as return values for MOCKED method calls
        //when repository.save are executed, then Mockito mock the return value of repository.save and return thenReturn() value,
        //we focus to test RestAPI here, so repository can be mocked.
        when(repository.save(Mockito.any(Product.class))).thenReturn(new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18));

        //
        // When
        //Execution
        //This basically only calls the tested method.
        //
        // Then
        //Verification
        //assertions on any results
        mvc.perform(post("/v1").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(product)))
                .andExpect(status().isOk());

        //MEMO: HamcrestのMatchers
        //https://qiita.com/opengl-8080/items/e57dab6e1fa5940850a3

       }

    @Test
    public void testFindAll() throws Exception {
        ArrayList<GenreType> genres = new ArrayList<GenreType>();
        genres.add(GenreType.Programming);
        genres.add(GenreType.GameDevelopment);
        genres.add(GenreType.Mobile);
        Product product = new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18);
        ArrayList<Product> products = new ArrayList<>();
        products.add(product);
        when(repository.findAll()).thenReturn(products);
        mvc.perform(get("/v1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindById() throws Exception {
        ArrayList<GenreType> genres = new ArrayList<GenreType>();
        genres.add(GenreType.Programming);
        genres.add(GenreType.GameDevelopment);
        genres.add(GenreType.Mobile);
        Product product = new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18);
        product.setId("1");
        when(repository.findById("1")).thenReturn(product);
        mvc.perform(get("/v1/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testFind() throws Exception {

        ArrayList<GenreType> genres = new ArrayList<GenreType>();
        genres.add(GenreType.Programming);
        genres.add(GenreType.GameDevelopment);
        genres.add(GenreType.Mobile);
        Product product1 = new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18);
        product1.setId("1");
        Product product2 = new Product("Learn the key features and implementation strategies of the Apache Camel Enterprise Integration framework",
                "Usama Wahab Khan",new Date(),"Learn the tips and tricks of creating analytical reports and dashboards for your business in no time.",
                genres,
                45);
        product2.setId("2");
        ArrayList<String> ids = new ArrayList<>();
        ids.add(product1.getId());
        ids.add(product2.getId());
        when(repository.findById("1")).thenReturn(product1);
        when(repository.findById("2")).thenReturn(product2);

        //https://github.com/eugenp/tutorials/blob/master/spring-boot/src/test/java/org/baeldung/demo/boottest/EmployeeRestControllerIntegrationTest.java
        mvc.perform(post("/v1/ids").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(ids)))
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(2))))
                .andExpect(jsonPath("$[0].id", is(product1.getId())))
                .andExpect(jsonPath("$[1].id", is(product2.getId())));


    }


    @Test
    public void testUpdate() throws Exception {
        ArrayList<GenreType> genres = new ArrayList<GenreType>();
        genres.add(GenreType.Programming);
        genres.add(GenreType.GameDevelopment);
        genres.add(GenreType.Mobile);
        Product product = new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18);
        when(repository.save(Mockito.any(Product.class))).thenAnswer(new Answer<Product>() {
            @Override
            public Product answer(InvocationOnMock invocation) throws Throwable {
                Product a = invocation.getArgumentAt(0, Product.class);
                return a;
            }
        });
        mvc.perform(put("/v1").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(product)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is(product.getName())));
    }

    @Test
    public void testFindByGenres() throws Exception {

        ArrayList<GenreType> genres = new ArrayList<GenreType>();
        genres.add(GenreType.Programming);
        genres.add(GenreType.GameDevelopment);
        genres.add(GenreType.Mobile);
        Product product = new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18);
        ArrayList<Product> products = new ArrayList<>();
        products.add(product);

        when(repository.findByGenre(GenreType.Programming)).thenReturn(products);

        //https://github.com/eugenp/tutorials/blob/master/spring-boot/src/test/java/org/baeldung/demo/boottest/EmployeeRestControllerIntegrationTest.java
       mvc.perform(post("/v1/genres").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(genres)))
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(1))))
               .andExpect(jsonPath("$[0].name", is("Applied Machine Learning with Python")));


    }

    @Test
    public void testFindByKeyword() throws Exception {
        ArrayList<GenreType> genres = new ArrayList<GenreType>();
        genres.add(GenreType.Programming);
        genres.add(GenreType.GameDevelopment);
        genres.add(GenreType.Mobile);
        Product product = new Product("Applied Machine Learning with Python",
                "By Hamidreza Sattari",new Date(), "To write the machine learning and deep learning applications that create your business edge",
                genres,
                18);
        ArrayList<Product> products = new ArrayList<>();
        products.add(product);
        when(repository.findByNameLike("Machine")).thenReturn(products);
        mvc.perform(get("/v1/keyword/Machine"))
                .andExpect(status().isOk());
    }

    //delete
    //https://stackoverflow.com/questions/47759833/spring-mockmvc-how-to-test-delete-request-of-rest-controller?rq=1
    @Test
    public void testDelete() throws Exception {
        this.mvc.perform(delete("/v1/{id}", "1").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}
