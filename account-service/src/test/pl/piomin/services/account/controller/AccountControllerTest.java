package pl.piomin.services.account.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;



import pl.piomin.services.account.model.Account;
import pl.piomin.services.account.repository.AccountRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//https://examples.javacodegeeks.com/core-java/junit/junit-mockito-when-thenreturn-example/
import static org.mockito.Mockito.when;
/*
public class ProductControllerTest {

}
*
*
*/

//MEMO: test examples
//https://github.com/harshulvarshney/SpringBoot_REST_Mongo/blob/master/sample-bird-app/src/test/java/com/harshul/sample_bird_app/BirdAppControllerTest.java

//THIS IS UNIT TEST
@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    MockMvc mvc;
    @MockBean
    AccountRepository repository;

    @Test
    public void testAdd() throws Exception {

		Account account = new Account("1234567812345678", 5000, "kyle nicole",928, "1");

        //MEMO:given-when-then
        //https://blog.codecentric.de/en/2017/09/given-when-then-in-junit-tests/
        //
        //MOCK: basically can mock component whose test have already passed before at somewhere else
        //
        //Given-When-Then test writing
        //
        // Given
        //Preparation
        //Here objects are created that are required as return values for MOCKED method calls
        //when repository.save are executed, then Mockito mock the return value of repository.save and return thenReturn() value,
        //we focus to test RestAPI here, so repository can be mocked.
        when(repository.save(Mockito.any(Account.class))).thenReturn(new Account("1234567812345678", 5000, "kyle nicole",928, "1"));

        //
        // When
        //Execution
        //This basically only calls the tested method.
        //
        // Then
        //Verification
        //assertions on any results
        mvc.perform(post("/v1").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(account)))
                .andExpect(status().isOk());

        //MEMO: HamcrestのMatchers
        //https://qiita.com/opengl-8080/items/e57dab6e1fa5940850a3

       }

    @Test
    public void testWithdraw() throws Exception {
        Account account = new Account("1234567812345678", 5000, "kyle nicole",928, "1");
        account.setId("1");
        when(repository.findById("1")).thenReturn(account);
        //thenAnswerの役目...
        //accountにbalanceを引くopが加わるので, withdraw中で実際に変更後のaccountをrepository.save()の引数として受け取る.
        when(repository.save(Mockito.any(Account.class))).thenAnswer(new Answer<Account>() {
            @Override
            public Account answer(InvocationOnMock invocation) throws Throwable {
                Account a = invocation.getArgumentAt(0, Account.class);
                return a;
            }
        });
        mvc.perform(put("/v1/withdraw/1/1000"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.balance", is(4000)));
    }



    @Test
    public void testUpdate() throws Exception {
        //Account account_before = new Account("1234567812345678", 5000, "kyle nicole",928, "1");
        //account_before.setId("1");
        Account account_after = new Account("8765432187654321", 5000, "kyle nicole",928, "1");
        account_after.setId("1");
        //thenAnswerの役目...
        //accountにbalanceを引くopが加わるので, withdraw中で実際に変更後のaccountをrepository.save()の引数として受け取る.
        when(repository.save(Mockito.any(Account.class))).thenAnswer(new Answer<Account>() {
            @Override
            public Account answer(InvocationOnMock invocation) throws Throwable {
                Account a = invocation.getArgumentAt(0, Account.class);
                return a;
            }
        });
        mvc.perform(put("/v1").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(account_after)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.number", is("8765432187654321")));
    }

    @Test
    public void testFindById() throws Exception {
        Account account = new Account("8765432187654321", 5000, "kyle nicole",928, "1");
        account.setId("1");
        when(repository.findById("1")).thenReturn(account);
        mvc.perform(get("/v1/1").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(account)))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindByCustomerId() throws Exception {
        Account account = new Account("8765432187654321", 5000, "kyle nicole",928, "1");
        ArrayList<Account> accounts = new ArrayList<Account>();
        accounts.add(account);
        when(repository.findByCustomerId("1")).thenReturn(accounts);
        mvc.perform(get("/v1/customer/1").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(account)))
                .andExpect(status().isOk());
    }



}
