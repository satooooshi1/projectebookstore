package pl.piomin.services.account.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import pl.piomin.services.account.model.Account;
import java.util.List;

//popular method now
import static org.assertj.core.api.Assertions.assertThat;
//obsolete method
//import static org.junit.Assert.*;


//MEMO: @DataMongoTest template
//https://github.com/wonwoo/spring-test-sample

//ATTENTION: Repository test && swagger
// when repository test && use @DateMongoTest, MUST comment out @Swagger annotation on Application && ApplicationTest Classes!!
@RunWith(SpringRunner.class)
@DataMongoTest
public class AccountRepositoryTest {

    //MEMO: private specifier
    //private make the repository stand-aloned( separated) from other repository ex. databaseloader
    //if not use private specifier, database loader's data has effect to these tests!!
    @Autowired
    private AccountRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    //MEMO: static private final
    //https://www.sejuku.net/blog/22426
    //staticなクラス変数は、処理の結果によって値が変わるのではなく、クラス毎で統一して持つ値になります。
    //
    //staticなクラスメソッド・クラス変数はそのクラスで共有して使われます。
    //
    //staticなクラス変数はそのクラスで共有して使われ、どこで呼び出しても変わらない値になります。

    //privateはそのクラス内のみで使われる

    //given
    private static Account account = new Account("8765432187654321", 5000, "kyle nicole",928, "1");
    @Before// each test
    public void dataSetup() {

        account.setId("1");
        repository.save(account);

    }

    @Test
    public void testFindByCustomerId() {
        //when
        List<Account> found = repository.findByCustomerId(account.getCustomerId());
        //then
        assertThat(found.get(0).getCustomerId()).isEqualTo(account.getCustomerId());
    }


    //MEMO: assertion example.
    //https://github.com/joel-costigliola/assertj-examples/blob/master/assertions-examples/src/test/java/org/assertj/examples/IterableAssertionsExamples.java
    //MEMO: list assertion example
    //https://github.com/joel-costigliola/assertj-examples/blob/master/assertions-examples/src/test/java/org/assertj/examples/ListSpecificAssertionsExamples.java
    @Test
    public void testFindById() {

        Account found = repository.findById(account.getId());
        assertThat(found.getId()).isEqualTo(account.getId());

    }

/*
    @Test
    public void mongoTemplateTest() {
        Account account = new Account("8765432187654321", 5000, "kyle nicole",928, "1");
        account.setId("1");
        mongoTemplate.save(account);
        List<Account> accounts = mongoTemplate.findAll(Account.class);
        assertThat(accounts.size()).isEqualTo(1);
        assertThat(accounts.get(0).getId()).isEqualTo("1");
    }
    */

}