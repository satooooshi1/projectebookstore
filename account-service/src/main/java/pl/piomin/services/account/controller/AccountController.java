package pl.piomin.services.account.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.piomin.services.account.contract.AccountService;
import pl.piomin.services.account.model.Account;
import pl.piomin.services.account.repository.AccountRepository;

@RestController
public class AccountController implements AccountService {

	@Autowired
	AccountRepository repository;

	public Account add(@RequestBody Account account) {
		//MEMO: this is to be patched!!
		account.setBalance(5000);
		return repository.save(account);
	}

	public Account update(@RequestBody Account account) {
		return repository.save(account);
	}

	//MEMO:
	//AccountController is the stub of the interfaces.
	//Check full path at the interfaces in contract.AccountService.java
	//
	//
	//
	public Account withdraw(@PathVariable("id") String id, @PathVariable("amount") int amount) {
		Account account = repository.findById(id);
		account.setBalance(account.getBalance() - amount);
		return repository.save(account);
	}

	public Account findById(@PathVariable("id") String id) {
		return repository.findById(id);
	}

	public List<Account> findByCustomerId(@PathVariable("customerId") String customerId) {
		return repository.findByCustomerId(customerId);
	}
/*
	public List<Account> find(@RequestBody List<String> ids) {
		List<Account> accounts= new ArrayList<>();
		for(String id : ids ){
			accounts.add(repository.findById(id));
		}
		return accounts;
	}
*/
	public void delete(@PathVariable("id") String id) {
		repository.delete(id);
	}

}
