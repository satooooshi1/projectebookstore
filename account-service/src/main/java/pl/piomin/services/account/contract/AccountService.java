package pl.piomin.services.account.contract;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import pl.piomin.services.account.model.Account;

public interface AccountService {

	@PostMapping("/v1")
	Account add(@RequestBody Account account);
	
	@PutMapping("/v1")
	Account update(@RequestBody Account account);
	
	@PutMapping("/v1/withdraw/{id}/{amount}")
	Account withdraw(@PathVariable("id") String id, @PathVariable("amount") int amount);
	
	@GetMapping("/v1/{id}")
	Account findById(@PathVariable("id") String id);
	
	@GetMapping("/v1/customer/{customerId}")
	List<Account> findByCustomerId(@PathVariable("customerId") String customerId);
	
	//@PostMapping("/v1/ids")
	//List<Account> find(@RequestBody List<String> ids);
	
	@DeleteMapping("/v1/{id}")
	void delete(@PathVariable("id") String id);
	
}
