package pl.piomin.services.account.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "account")
public class Account {

	@Id
	private String id;
	private String number;
	private int balance;
	private String name;
	private int expire;
	private String customerId;

	public Account() {

	}
	
	public Account(String number, int balance, String customerId) {
		this.number = number;
		this.balance = balance;
		this.customerId = customerId;
	}

	public Account(String number, int balance, String name, int expire, String customerId){

		this.number = number;
		this.balance = balance;
		this.name = name;
		this.expire = expire;
		this.customerId = customerId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public String getName(){
		return name;
	}

	public void setName(){
		this.name=name;
	}

	public int getExpire(){
		return expire;
	}

	public void setExpire(int expire){
		this.expire = expire;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}

