package pl.piomin.services.account;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.piomin.services.account.model.Account;
import pl.piomin.services.account.repository.AccountRepository;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.FileReader;
import java.io.IOException;

//@EnableSwagger2
@SpringBootApplication
@EnableDiscoveryClient
public class AccountApplication {

	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(AccountApplication.class).web(true).run(args);
	}
/*
	@Autowired
	AccountRepository repository;

	@Bean
	AccountRepository repository() {
		//AccountRepository repository = new AccountRepository();
		repository.save(new Account("1234567812345671", 5000, "kyle nicole",928, "1000"));
		repository.save(new Account("1234567891234562", 5000, "kyle nicole",1228,"1000"));
		repository.save(new Account("1234567891234563", 5000, "kyle nicole",328,"1000"));
		repository.save(new Account("1234567891234564", 5000, "kyle nicole",1228,"1000"));
		repository.save(new Account("1234567891234565", 5000, "kyle nicole",1228,"1000"));
		repository.save(new Account("1234567891234566", 5000, "kyle nicole",1228,"1000"));
		repository.save(new Account("1234567891234567", 5000, "kyle nicole",1228,"1000"));
		repository.save(new Account("1234567891234568", 5000, "kyle nicole",1228,"1000"));
		repository.save(new Account("1234567891234569", 5000, "kyle nicole",1228,"1000"));
		return repository;
	}
*/
	//MEMO:
	//Added @EnableSwagger2 annotation at the top
	//should annotate @RequestMapping("/account") before controller
	//http://localhost:2222/swagger-ui.html
	//http://localhost:2222/v2/api-docs
	@Bean
	public Docket api() throws IOException, XmlPullParserException {
		MavenXpp3Reader reader = new MavenXpp3Reader();
		Model model = reader.read(new FileReader("pom.xml"));
		ApiInfoBuilder builder = new ApiInfoBuilder()
				.title("Account Service Api Documentation")
				.description("Documentation automatically generated")
				.version(model.getVersion())
				.contact(new Contact("satoshi", "satoshi.com", "satoshi@gmail.com"));
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("pl.piomin.services.account.controller"))
				.paths(PathSelectors.any()).build()
				.apiInfo(builder.build());
	}

	@Bean
	UiConfiguration uiConfig() {
		return new UiConfiguration("validatorUrl", "list", "alpha", "schema",
				UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS, false, true, 60000L);
	}

	//MEMO: cors setting
	//https://spring.io/blog/2015/06/08/cors-support-in-spring-framework
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				//registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:3000");
				registry.addMapping("/**")
						.allowedOrigins("http://localhost:3000")
						.allowedMethods("GET","POST","PUT", "DELETE");
			}
		};
	}

}
