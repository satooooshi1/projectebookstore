package pl.piomin.services.customer.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.piomin.services.cart.model.Cart;
import pl.piomin.services.cart.model.CartType;
import pl.piomin.services.cart.repository.CartRepository;
import pl.piomin.services.customer.model.Customer;
import pl.piomin.services.customer.model.CustomerType;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@DataMongoTest
public class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository repository;

    //given
    private static Customer customer = new Customer("John Scott","john@qq.com","john", CustomerType.NEW);
    @Before// each test
    public void dataSetup() {

        customer.setId("1");
        repository.save(customer);

    }

    @Test
    public void testFindAll() {
        List<Customer> found=repository.findAll();
        assertThat(found.get(0).getId()).isEqualTo(customer.getId());
    }

    @Test
    public void testFindById() {
        Customer found=repository.findById(customer.getId());
        assertThat(found.getId()).isEqualTo(customer.getId());
    }

    @Test
    public void testFindByEmailAndPassword() {
        Customer found=repository.findByEmailAndPassword(customer.getEmail(), customer.getPassword());
        assertThat(found.getId()).isEqualTo(customer.getId());
    }
}