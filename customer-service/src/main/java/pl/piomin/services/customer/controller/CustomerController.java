package pl.piomin.services.customer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.piomin.services.account.model.Account;
import pl.piomin.services.customer.client.AccountClient;
import pl.piomin.services.customer.contract.CustomerService;
import pl.piomin.services.customer.model.Customer;
import pl.piomin.services.customer.model.CustomerType;
import pl.piomin.services.customer.repository.CustomerRepository;
import pl.piomin.services.product.model.Product;

@RestController
public class CustomerController implements CustomerService {

	@Autowired
	AccountClient accountClient;
	@Autowired
	CustomerRepository repository;
	
	public Customer add(@RequestBody Customer customer) {
		customer.setType(CustomerType.NEW);
		return repository.save(customer);
	}

	public Customer logIn(@RequestBody Customer customer) {
		Customer logInUser=repository.findByEmailAndPassword(customer.getEmail(), customer.getPassword());
		return logInUser;
	}
	
	public Customer update(@RequestBody Customer customer) {
		return repository.save(customer);
	}
	
	public Customer findById(@PathVariable("id") String id) {
		return repository.findById(id);
	}
	//MEMO: CustomerController.java (controller : Customer-service) 
	//		-> accountClient.java@FeignClient (clientで輸入するservice定義 : customer-service )
	//		->  accountService.java (contractで輸出するservice定義 : account-service) -> findById

	public Customer findByIdWithAccounts(@PathVariable("id") String id) {
		List<Account> accounts = accountClient.findByCustomerId(id);
		Customer c = repository.findById(id);
		c.setAccounts(accounts);
		return c;
	}

	/*
	public List<Product> findByIdWithProducts(@RequestBody List<Long> ids){

		//ids are the cart entities with customerId
		List<Product> products = productClient.find(ids);
		Customer c = repository.findById(id);
		c.setAccounts(accounts);
		return products;
	}
	*/
	/*
	public List<Customer> find(@RequestBody List<String> ids) {
		return repository.find(ids);
	}
	*/
	public void delete(@PathVariable("id") String id) {
		repository.delete(id);
	}
	
}
