package pl.piomin.services.customer.contract;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import pl.piomin.services.customer.model.Customer;

public interface CustomerService {

	@PostMapping("/v1/login")
	public Customer logIn(@RequestBody Customer customer);

	@PostMapping("/v1")
	Customer add(@RequestBody Customer customer);
	
	@PutMapping("/v1")
	Customer update(@RequestBody Customer customer);
	
	@GetMapping("/v1/{id}")
	Customer findById(@PathVariable("id") String id);
	
	@GetMapping("/v1/withAccounts/{id}")
	Customer findByIdWithAccounts(@PathVariable("id") String id);
	
	//@PostMapping("/customer/ids")
	//List<Customer> find(@RequestBody List<String> ids);
	
	@DeleteMapping("/v1/{id}")
	void delete(@PathVariable("id") String id);
	
}
