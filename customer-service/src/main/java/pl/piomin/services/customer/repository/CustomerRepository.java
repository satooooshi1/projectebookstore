package pl.piomin.services.customer.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import pl.piomin.services.customer.model.Customer;
import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.model.Product;

/*
public class CustomerRepository {

	private List<Customer> customers = new ArrayList<>();
	
	public Customer add(Customer customer) {
		customer.setId((long) (customers.size()+1));
		customers.add(customer);
		return customer;
	}
	
	public Customer update(Customer customer) {
		customers.set(customer.getId().intValue() - 1, customer);
		return customer;
	}
	
	public Customer findById(Long id) {
		Optional<Customer> customer = customers.stream().filter(p -> p.getId().equals(id)).findFirst();
		if (customer.isPresent())
			return customer.get();
		else
			return null;
	}
	
	public void delete(Long id) {
		customers.remove(id.intValue());
	}
	
	public List<Customer> find(List<Long> ids) {
		return customers.stream().filter(p -> ids.contains(p.getId())).collect(Collectors.toList());
	}
	
}
*/


public interface CustomerRepository extends MongoRepository<Customer, String> {

	public List<Customer> findAll();
	public Customer findById(String id);
	public Customer findByEmailAndPassword(String email, String password);
	//public List<Customer> find(List<String> ids);
}
