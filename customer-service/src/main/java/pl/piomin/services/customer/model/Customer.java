package pl.piomin.services.customer.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.piomin.services.account.model.Account;
//MEMO: import from another project
//added cart-service in customer-service.iml
import pl.piomin.services.cart.model.Cart;


/*	Cart entity

    private Long id;
    private Long customerId;
    private Long productId;
    private CartType type;

* */
@Document(collection = "customer")
public class Customer {

	@Id
	private String id;
	private String name;
	private String email;
	private String password;
	private CustomerType type;
	private List<Account> accounts = new ArrayList<>();

	public Customer() {

	}
	
	public Customer(String name, CustomerType type) {
		this.name = name;
		this.type = type;
	}

	public Customer(String name, String email, CustomerType type) {
		this.name = name;
		this.email = email;
		this.type = type;
	}

	public Customer(String name, String email, String password, CustomerType type) {

		this.name = name;
		this.email = email;
		this.password = password;
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail(){
		return email;
	}

	public void setEmail(String email){
		this.email=email;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password=password;
	}

	public CustomerType getType() {
		return type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

}
