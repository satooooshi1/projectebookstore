package pl.piomin.services.order.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.piomin.services.product.model.Product;

import java.util.List;
import java.util.Date;

@Document(collection = "order")
public class Order {

	@Id
	private String id;
	private OrderStatus status;
	private int price;
	private String customerId;
	private String accountId;
	private List<String> productIds;
	//private List<Product> products;
	private Date placedDate;//[NEW, PROCESSING, ACCEPTED, DONE, REJECTED;]

	//MEMO: patch for junit error
	//There was a custom constructor defined for the class making it the default constructor. Introducing a dummy constructor has made the error to go away:
	//https://stackoverflow.com/questions/7625783/jsonmappingexception-no-suitable-constructor-found-for-type-simple-type-class
	public Order(){}

	public Order(OrderStatus status, int price, String customerId, String accountId, List<String> productIds){

		this.status = status;
		this.price = price;
		this.customerId = customerId;
		this.accountId = accountId;
		this.productIds = productIds;
	}

	public Order(OrderStatus status, int price, String customerId, String accountId, List<String> productIds, Date placedDate){

		this.status = status;
		this.price = price;
		this.customerId = customerId;
		this.accountId = accountId;
		this.productIds = productIds;
		this.placedDate = placedDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds)
	{
		this.productIds = productIds;
	}

	public Date getPalcedDate(){
		return placedDate;
	}

	public void setAcceptedDate(Date placedDate){
		this.placedDate = placedDate;
	}

}

