package pl.piomin.services.order.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import pl.piomin.services.account.model.Account;
import pl.piomin.services.customer.model.Customer;
import pl.piomin.services.order.client.AccountClient;
import pl.piomin.services.order.client.CustomerClient;
import pl.piomin.services.order.client.ProductClient;
import pl.piomin.services.order.model.Order;
import pl.piomin.services.order.model.OrderStatus;
import pl.piomin.services.order.repository.OrderRepository;
import pl.piomin.services.product.model.Product;

@RestController
public class OrderController {

//	private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
	
	@Autowired
	OrderRepository repository;	
	@Autowired
	AccountClient accountClient;
	@Autowired
	CustomerClient customerClient;
	@Autowired
	ProductClient productClient;


	@GetMapping("/v1")
	public List<Order>findAll(){
		return repository.findAll();
	}

	@GetMapping("/v1/{customerId}")
	public List<Order>findByCustomerId(@PathVariable String customerId){
		return repository.findByCustomerId(customerId);
	}

	@PostMapping("/v1")
	public Order prepare(@RequestBody Order order) {
		//MEMO: necessary fields by frontend
		//productIds, customerId, accountId
		int price = 0;
		List<Product> products = productClient.find(order.getProductIds());
		Customer customer = customerClient.findByIdWithAccounts(order.getCustomerId());
		//Customer customer = customerClient.findById(order.getCustomerId());
		for (Product product : products) 
			price += product.getPrice();
		final int priceDiscounted = priceDiscount(price, customer);
		//TODO:
		//midify to find the account card number(accountId)
		Optional<Account> account = customer.getAccounts().stream().filter(a -> (a.getBalance() > priceDiscounted)).findFirst();

		if (account.isPresent()) {
			order.setAccountId(account.get().getId());
			order.setStatus(OrderStatus.ACCEPTED);
			order.setPrice(priceDiscounted);
		} else {
			order.setStatus(OrderStatus.REJECTED);
		}
		Date date=new Date();
		order.setAcceptedDate(date);
		return repository.save(order);
	}

	//payment occur here!!
	@PutMapping("/v1/{id}")
	public Order accept(@PathVariable String id) {
		final Order order = repository.findById(id);

		//
		//My imple. find and substitute the price from account
		//
		//test case: balance insufficient!!
		//
		Account payment = accountClient.findById(order.getAccountId());
		int balance = payment.getBalance();
		int priceDiscounted = order.getPrice();
		if(balance < priceDiscounted ){
			order.setStatus(OrderStatus.REJECTED);
			repository.save(order);
		}

		accountClient.withdraw(order.getAccountId(), order.getPrice());
		order.setStatus(OrderStatus.DONE);
		order.setStatus(OrderStatus.DONE);
		repository.save(order);
		return order;
	}

	@GetMapping("/v1/history/{start}/{end}")
	public List<Order>findByDateBetween(@PathVariable Date start, @PathVariable Date end){
		return repository.findAllByPlacedDateTimeBetween(start, end);
	}

	
	private int priceDiscount(int price, Customer customer) {
		double discount = 0;
		switch (customer.getType()) {
		case REGULAR:
			discount += 0.05;
			break;
		case VIP:
			discount += 0.1;
			break;
			
		default:
			break;
		}
		int ordersNum = repository.countByCustomerId(customer.getId());
		discount += (ordersNum*0.01);
		return (int) (price - (price * discount));
	}


	
}
