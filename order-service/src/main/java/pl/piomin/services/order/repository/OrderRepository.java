package pl.piomin.services.order.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import pl.piomin.services.order.model.Order;
import pl.piomin.services.product.model.GenreType;
import pl.piomin.services.product.model.Product;

/*
public class OrderRepository {

	private List<Order> orders = new ArrayList<>();
	
	public Order save(Order order) {
		order.setId((long) (orders.size()+1));
		orders.add(order);
		return order;
	}
	
	public Order update(Order order) {
		orders.set(order.getId().intValue() - 1, order);
		return order;
	}
	
	public Order findById(Long id) {
		Optional<Order> order = orders.stream().filter(p -> p.getId().equals(id)).findFirst();
		if (order.isPresent())
			return order.get();
		else
			return null;
	}
	
	public void delete(Long id) {
		orders.remove(id.intValue());
	}
	
	public List<Order> find(List<Long> ids) {
		return orders.stream().filter(p -> ids.contains(p.getId())).collect(Collectors.toList());
	}
	
	public int countByCustomerId(String customerId) {
		return (int) orders.stream().filter(p -> p.getCustomerId().equals(customerId)).count();
	}
}
*/
public interface OrderRepository extends MongoRepository<Order, String> {
	public Order findById(String id);
	public List<Order> findByCustomerId(String customerId);
	public int countByCustomerId(String customerId);
	public List<Order> findAllByPlacedDateTimeBetween(Date orderPlacedTimeStart,
													  Date orderPlacedTimeEnd);
}

